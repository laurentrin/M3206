#!/bin/sh
#Check internet
wget -q --spider http://google.com
if [ $? -eq  0 ]; then
	echo "[...]Checking internet connection [...]"
	echo "[...]Internet access OK           [...]"
else
	echo "[...]Checking internet connection [...]"
	echo "[/!\]Not connected to Internet    [/!\]"
	echo "[/!\]Please check configuration   [/!\]"
fi
#####################################
#Check ssh
dpkg -l | grep ssh > /dev/null
if [ $? -eq 0 ]; then
	service ssh status > /dev/null #Verifie si le daemon ssh est lance et on met le resultat dans /dev/null qui detruit automatiquement son contenu
	if [ $? -eq 0 ]; then #Si le daemon est en marche alors on affiche le message suivant
		echo "[...] ssh: fonctionne     [...]"
	else #sinon on le lance 
		echo "[...] ssh: est installé               [...]"
		echo "[/!\] ssh: le service n'est pas lancé  /!\]"
		echo "[...] ssh: lancement du service        [/!\] lancer la commande: `/etc/init.d/ssh start`"
	fi
else 
	echo "[...] ssh: pas installé              [...] lancer la commande: `apt-get install ssh` "
fi
#####################################
#Check Tools
tools="git tmux vim htop" #Tableau des outils que l on veut verifier

for i in $tools ; do #Parcours le tableau d'outils
	dpkg -l | grep $i > /dev/null #Execution de la commande 
	if [ $? -eq 0 ]; then #Si la commande precedente reussi alors $? retourne 0
		echo "[...] $i : installé [...]"
	else
		echo "[/!\] $i : pas installé /!\]lancer la commande: `apt-get install $i` "
fi
done
#####################################
#Update System
if [ "$(whoami)" = "root" ]; then
	apt-get update > /dev/null #Pour ne pas afficher le contenu de la commande dans le terminal
	echo "[...] update database [...]"
	apt-get upgrade > /dev/null #Cela �crase les lignes du fichiepr�c�dent
	echo "[...] update system   [...]"
else
	echo "[/!\] Vous devez etre en super-utilisateur/!"
fi
