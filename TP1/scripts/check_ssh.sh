#!/bin/sh
dpkg -l | grep ssh > tmp
if [ $? -eq 0 ]; then
	service ssh status > tmp #Verifie si le daemon ssh est lance et on met le resultat dans un fichier temporaire
	if [ $? -eq 0 ]; then #Si le daemon est en marche alors on affiche le message suivant
		echo "[...] ssh: fonctionne     [...]"
	else #sinon on le lance 
		echo "[...] ssh: est installé               [...]"
		echo "[/!\] ssh: le service n'est pas lancé  /!\]"
		echo "[...] ssh: lancement du service        [/!\] lancer la commande: `/etc/init.d/ssh start`"
	fi
else 
	echo "[...] ssh: pas installé              [...] lancer la commande: `apt-get install ssh` "
fi
rm -f tmp #suppresion du fichier tmp
