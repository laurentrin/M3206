#!/bin/sh
tools="git tmux vim htop" #Tableau des outils que l on veut verifier

for i in $tools ; do #Parcours le tableau d'outils
	dpkg -l | grep $i > tmp #Execution de la commande et ajout du resultat dans un fichier temporaire 
	if [ $? -eq 0 ]; then #Si la commande precedente reussi alors $? retourne 0
		echo "[...] $i : installé [...]"
	else
		echo "[/!\] $i : pas installé /!\]lancer la commande: `apt-get install $i` "
fi
done
rm -f tmp
