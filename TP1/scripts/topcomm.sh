#!/bin/sh
re='^[0-9]+$'
        if ! [[ $1 =~ $re ]] ; then #Verifie que l'utilisateur tape bien un nombre
           echo "error: $1 not a number" >&2
        else
          awk '{print $1}' ~/.bash_history | sort | uniq -c | sort -rn | head -n $1
 	 fi
