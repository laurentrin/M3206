#!/bin/bash
[[ -d $1 ]]
if [[ $? -eq 0 ]]; then
nom=$(date +%Y_%m_%d_%H%M)_$(basename $1).tar.gz
tar cvzf $nom $1 > /dev/null
echo "creation de l'archive : $nom"
fi
