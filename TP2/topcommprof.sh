#!/bin/bash
re='^[0-9]+$'
        if ! [[ $1 =~ $re ]] ; then #Verifie que l'utilisateur tape bien un nombre
           echo "error: $1 not a number" >&2
        else
          awk 'BEGIN{FS=";"}''{print $2}' my_history | awk '{print $1}' | sort | uniq -c | sort -rn | head -n $1
 	 fi
